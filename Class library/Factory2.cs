﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace Task11
    {
        public class Factory2 : IFactory
        {
            ProductA2 a;
            ProductB2 b;
            public IProductA GetProductA()
            {
                return a;
            }

            public IProductB GetProductB()
            {
                return b;
            }
        }
    }
}
