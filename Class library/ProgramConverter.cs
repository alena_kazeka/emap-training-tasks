﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    public class ProgramConverter: IConvertible
    {
        public string ConvertToCSharp(string text)
        {
            if (text == null)
                throw new ArgumentNullException();
            return text;
        }

        public string ConvertToVB(string text)
        {
            if (text == null)
                throw new ArgumentNullException();
            return text;
        }
    }
}
