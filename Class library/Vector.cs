﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpamTrainingClassLibrary.MyException.FigureException;

namespace EpamTrainingClassLibrary
{
    namespace Figure
    {
        public class Vector
        {
            Point top;

            public Point Top
            {
                get { return top; }
                set { top = value; }
            }

            public Vector(Point _top)
            {
                top = _top;
            }
            public Vector(int topX, int topY, int topZ = 0)
            {
                top = new Point(topX, topY, topZ);
            }

            public int this[string point]
            {
                get
                {
                    switch(point)
                    {
                        case "x":
                            return top.x;
                        case "y":
                            return top.y;
                        case "z":
                            return top.z;
                        default:
                            throw new VectorException(ExceptionResource.IncorrectIndex);
                    }
                }
                set
                {
                    switch (point)
                    {
                        case "x":
                            top.x = value;
                            break;
                        case "y":
                            top.y = value;
                            break;
                        case "z":
                            top.z = value;
                            break;
                        default:
                            throw new VectorException(ExceptionResource.IncorrectIndex);
                    }
                }
            }

            public int this[int index]
            {
                get
                {
                    if (index == 0)
                        return top.x;
                    else if (index == 1)
                        return top.y;
                    else if (index == 2)
                        return top.z;
                    throw new VectorException(ExceptionResource.IncorrectIndex);
                }
                set
                {
                    if (index == 0)
                        top.x = value;
                    else if (index == 1)
                        top.y = value;
                    else if (index == 2)
                        top.z = value;
                    else
                        throw new VectorException(ExceptionResource.IncorrectIndex);
                }
            }

            public static Vector operator +(Vector vectorA, Vector vectorB)
            {
                if (vectorA == null || vectorB == null)
                    throw new ArgumentNullException();
                return new Vector(vectorA.top.x + vectorB.top.x, vectorA.top.y + vectorB.top.y, vectorA.top.z + vectorB.top.z);
            }

            public static Vector operator -(Vector vectorA, Vector vectorB)
            {
                if (vectorA == null || vectorB == null)
                    throw new ArgumentNullException();
                return new Vector(vectorA.top.x - vectorB.top.x, vectorA.top.y - vectorB.top.y, vectorA.top.z - vectorB.top.z);
            }

            /// <summary>
            /// Scalar product of vectors
            /// </summary>
            /// <param name="vectorA">First vector</param>
            /// <param name="vectorB">Second vector</param>
            /// <returns></returns>
            public static int operator *(Vector vectorA, Vector vectorB)
            {
                if (vectorA == null || vectorB == null)
                    throw new ArgumentNullException();
                return (vectorB.top.x * vectorA.top.x + vectorA.top.y * vectorB.top.y + vectorA.top.z * vectorB.top.z);
            }

            public static Vector operator *(Vector vectorA, int a)
            {
                if (vectorA == null)
                    throw new ArgumentNullException();
                return new Vector(a * vectorA.top.x, a * vectorA.top.y, a * vectorA.top.z);
            }

            public static Vector operator *(int a, Vector vectorA)
            {
                return (vectorA * a);
            }

            
            /// <summary>
            /// Calculates the mod of vector.
            /// </summary>
            /// <returns>The mod of vector.</returns>
            public double mod()
            {
                if (this == null)
                    throw new ArgumentNullException();
                return Math.Sqrt(this["x"] * this["x"] + this["y"] * this["y"] + this["z"] * this["z"]);
            }

            /// <summary>
            /// The angle between two vectors
            /// </summary>
            /// <param name="vectorA"></param>
            /// <param name="vectorB"></param>
            /// <returns></returns>
            public static double operator^(Vector vectorA, Vector vectorB)
            {
                double modA = vectorA.mod();
                double modB = vectorB.mod();
                double cos = vectorA * vectorB / (modA * modB);
                return (Math.Acos(cos)*180/Math.PI);
            }

            public static bool operator==(Vector vectorA, Vector vectorB)
            {
                if (Equals(vectorA, null) || Equals(vectorB, null))
                    throw new ArgumentNullException();
                if (vectorA.top == vectorB.top)
                    return true;
                return false;
            }

            public static bool operator !=(Vector vectorA, Vector vectorB)
            {
                return !(vectorA == vectorB);
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                    return false;
                if (!(obj is Vector))
                    return false;
                return (this == obj as Vector);
            }
        }
    }
}
