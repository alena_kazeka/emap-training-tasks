﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace MyException
    {
        namespace Task9Exception
        {
            public class FileChangeException: Exception
            {
                public FileChangeException(string message) : base(message) { }
            }
        }
    }
}
