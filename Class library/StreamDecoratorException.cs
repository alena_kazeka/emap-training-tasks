﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace MyException
    {
        public class StreamDecoratorException: Exception
        {
            public StreamDecoratorException(string message) : base(message) { }
        }
    }
}
