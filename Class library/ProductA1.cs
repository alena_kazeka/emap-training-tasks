﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpamTrainingClassLibrary.Task11;

namespace EpamTrainingClassLibrary
{
    namespace Task11
    {
        public class ProductA1: IProductA
        {
            string name;
            double price;
            public double Price
            {
                get { return price; }
                set { price = value; }
            }
            public string Name
            {
                get { return name; }
            }

            public ProductA1(string name)
            {
                this.name = name;
            }

            public double DiscountPrice(double discount)
            {
                return (price - discount * price);
            }
        }
    }
}
