﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using EpamTrainingClassLibrary.MyException;

namespace EpamTrainingClassLibrary
{
    /// <summary>
    /// The StreamDecorator class allows read part data from file.
    /// </summary>
    public class StreamDecorator
    {
        /// <summary>
        ///The password allows access to the file
        /// </summary>
        string password;

        /// <summary>
        /// The path to file 
        /// </summary>
        string filePath;

        public string FilePath
        {
            get { return filePath; }
        }

        /// <summary>
        /// The number of bytes are in the file with path filePath
        /// </summary>
        long lengthOfFile;

        public long LengthOfFile
        {
            get { return lengthOfFile; }
        }

        /// <summary>
        /// The number of bytes are in one percent of file length 
        /// </summary>
        double percentLength;

        public double PercentLength
        {
            get { return percentLength; }
        }

        /// <summary>
        /// The current position in file
        /// </summary>
        long position;

        public long Position
        {
            get { return position; }
        }

        /// <summary>
        /// The amount of data read in percent
        /// </summary>
        double readPercent;

        public double ReadPercent
        {
            get { return readPercent; }
        }


        /// <summary>
        /// Constructs a StreamDecorator instance with an initial filePath and password
        /// </summary>
        /// <param name="filePath">The path to file</param>
        /// <param name="password">The password to access the file</param>
        public StreamDecorator(string filePath, string password)
        {
            if (filePath == null)
                throw new ArgumentNullException(ExceptionResource.NullFilePath);
            if (password == null)
                throw new ArgumentNullException(ExceptionResource.NullPassword);
            
            InitializeMembers(filePath, password);         
        }

        /// <summary>
        /// Initialize members of class
        /// </summary>
        /// <param name="filePath">The path to file</param>
        /// <param name="password">The password to access the file</param>
        private void InitializeMembers(string filePath, string password)
        {
            passwordVerifieded = false;

            this.password = password;
            this.filePath = filePath;
  
            if (!File.Exists(filePath))
                throw new StreamDecoratorException(ExceptionResource.FileNotExists);

            try
            {
                FileStream stream = File.Open(filePath, FileMode.Open);
                lengthOfFile = stream.Length;
                stream.Close();

                percentLength = (double)lengthOfFile / 100;
                position = 0;
                readPercent = 0;
            }
            catch (FileLoadException ex)
            {
                throw new StreamDecoratorException(ex.Message);
            }
        }

        /// <summary>
        /// Checks the password to access the file
        /// </summary>
        /// <param name="password"></param>
        /// <returns>true: This password is the same as the present, false: This password does not match with the present</returns>
        public bool CheckPassword(string password)
        {
            if (password == null)
                throw new ArgumentNullException(ExceptionResource.NullPassword);

            passwordVerifieded = this.password != password;

            return passwordVerifieded;
        }

        bool passwordVerifieded;

        /// <summary>
        /// Read data from file
        /// </summary>
        /// <param name="needReadPercent">The percent value of file length, that must be read from file</param>
        /// <returns>The read data</returns>
        public string Read(double needReadPercent)
        {
            if (!passwordVerifieded)
                throw new StreamDecoratorException(ExceptionResource.PasswordNotVerified);

            if (needReadPercent > 100 || needReadPercent < 0 || needReadPercent > (100 - readPercent))
                throw new StreamDecoratorException(ExceptionResource.PercentValueException);
            
            FileStream stream = File.Open(filePath, FileMode.Open);
            stream.Position = position;
            int count = (int)Math.Round(needReadPercent*percentLength);
            byte [] byteArray = new byte[count];
            stream.Read(byteArray, 0, count);
            stream.Close();

            readPercent += needReadPercent;
            position += (long)Math.Round(needReadPercent * percentLength);

            return Encoding.UTF8.GetString(byteArray);
        }
    }
}
