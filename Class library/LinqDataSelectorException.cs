﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace MyException
    {
        namespace Task12Exception
        {
            public class LinqDataSelectorException : Exception
            {
                public LinqDataSelectorException(string message) : base(message) { }
            }
        }
    }
}
