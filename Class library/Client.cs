﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace Task11
    {
        public class Client
        {
            IProductA a;
            IProductB b;

            public Client(IFactory factory)
            {
                a = factory.GetProductA();
                b = factory.GetProductB();
            }
        }
    }
}
