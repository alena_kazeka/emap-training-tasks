﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace Converter
    {
        class BinaryString
        {
            public static StringBuilder UintToBinaryString(uint number)
            {
                StringBuilder resultString = new StringBuilder();
                uint residue = number;
                while (number > 0)
                {
                    residue >>= 1;
                    resultString.Insert(0, number - residue * 2);
                    number = residue;
                }

                return resultString;
            }
        }
    }
}
