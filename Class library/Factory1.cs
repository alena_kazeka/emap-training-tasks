﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpamTrainingClassLibrary.Task11;

namespace EpamTrainingClassLibrary
{
    namespace Task11
    {
        public class Factory1: IFactory
        {
            ProductA1 a;
            ProductB1 b;
            public IProductA GetProductA()
            {
                return a;
            }
            public IProductB GetProductB()
            {
                return b;
            }
        }
    }
}
