﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpamTrainingClassLibrary.MyException.MathException;

namespace EpamTrainingClassLibrary
{
    namespace Mathematic
    {
        /// <summary>
        /// The Martix class allows create matrix and make different operation of it.
        /// </summary>
        public class Matrix: IComparable
        {
            public int CompareTo(object obj)
            {
                if (obj == null)
                    throw new ArgumentNullException(ExceptionResource.NullObjectInCompare);
                if (!(obj is Matrix))
                    throw new ArgumentNullException(ExceptionResource.ObjectIsNotMatrixInstance);

                int valuesNumThis = Rows * Columns;
                int valuesNumObj = ((Matrix)obj).Rows * ((Matrix)obj).Columns;

                if (valuesNumThis == valuesNumObj)
                    return 0;
                if (valuesNumThis > valuesNumObj)
                    return 1;
                return -1;
            }

            /// <summary>
            /// Allows create instance of Matrix class
            /// </summary>
            /// <param name="matrixArray">Initialaze elements of matrix</param>
            public Matrix(double [,] matrixArray)
            {
                if (matrixArray == null)
                    throw new ArgumentNullException(ExceptionResource.NullArrayReference);

                this.matrixArray = (double[,])matrixArray.Clone();
            }

            /// <summary>
            /// Allows create instance of Matrix class
            /// </summary>
            /// <param name="rows">The Number of rows in matrix</param>
            /// <param name="columns">The Number of columns in matrix</param>
            /// <param name="value">The value that inizialize matrix</param>
            public Matrix(int rows, int columns, double value = 0)
            {
                if (rows < 1 || columns < 1)
                    throw new MatrixException(ExceptionResource.IncorrectRowsColumsValue);

                matrixArray = new double[rows, columns];
                for (int i = 0; i < rows; i++)
                    for (int j = 0; j < columns; j++)
                        matrixArray[i, j] = value;
            }

            /// <summary>
            /// Elements of matrix
            /// </summary>
            double[,] matrixArray;

            public double[,] MatrixArray
            {
                get 
                { 
                    return (double[,])matrixArray.Clone(); 
                }
            }

            /// <summary>
            /// Count of matrix rows
            /// </summary>
            public int Rows
            {
                get { return matrixArray.GetLength(0); }
            }

            /// <summary>
            /// Count of matrix columns
            /// </summary>
            public int Columns
            {
                get { return matrixArray.GetLength(1); }
            }

            /// <summary>
            /// Indexator for Matrix instance
            /// </summary>
            /// <param name="row">Row number</param>
            /// <param name="column">Column number</param>
            /// <returns></returns>
            public double this[int row, int column]
            {
                get
                {
                    if (row < 0 || row >= Rows || column < 0 || column >= Columns)
                        throw new MatrixException(ExceptionResource.IncorrectRowColumnInIndexator);

                    return matrixArray[row, column];
                }
                set
                {
                    if (row < 0 || row >= Rows || column < 0 || column >= Columns)
                        throw new MatrixException(ExceptionResource.IncorrectRowColumnInIndexator);

                    matrixArray[row, column] = value;
                }
            }

            public static Matrix operator *(double value, Matrix matrix)
            {
                if (ReferenceEquals(matrix, null))
                    throw new ArgumentNullException(ExceptionResource.NullMatrixReference);

                double[,] array = new double[matrix.Rows, matrix.Columns];
                for (int i = 0; i < matrix.Rows; i++)
                    for (int j = 0; j < matrix.Columns; j++)
                        array[i, j] = matrix[i, j] * value;
                return new Matrix(array);
            }

            public static Matrix operator *(Matrix matrix, double value)
            {
                return value * matrix;
            }

            public static Matrix operator *(Matrix matrix1, Matrix matrix2)
            {
                if (ReferenceEquals(matrix1, null) || ReferenceEquals(matrix2, null))
                    throw new ArgumentNullException(ExceptionResource.NullMatrixReference);

                if (matrix1.Columns != matrix2.Rows)
                    throw new MatrixException(ExceptionResource.NotEqualsRowsAndColumsForMultiplication);

                int rowsNum = matrix1.Rows;
                int columsNum = matrix2.Columns;
                double[,] array = new double[rowsNum, columsNum];
                double tempValue = 0;
                for (int i = 0; i < rowsNum; i++)
                    for (int j = 0; j < columsNum; j++)
                    {
                        for (int k = 0; k < matrix2.Rows; k++)
                            tempValue += matrix1[i, k] * matrix2[k, j];
                        array[i, j] = tempValue;
                        tempValue = 0;
                    }

                return new Matrix(array);
            }

            public static Matrix operator +(Matrix matrix1, Matrix matrix2)
            {
                if (ReferenceEquals(matrix1, null) || ReferenceEquals(matrix2, null))
                    throw new ArgumentNullException(ExceptionResource.NotEqualsRowsAndColumsForMultiplication);

                if (matrix1.Rows != matrix2.Rows || matrix1.Columns != matrix2.Columns)
                    throw new MatrixException(ExceptionResource.NotEqualsRowsAndColums);

                double[,] array = new double[matrix1.Rows, matrix1.Columns];
                for (int i = 0; i < matrix1.Rows; i++)
                    for (int j = 0; j < matrix1.Columns; j++)
                        array[i, j] = matrix1[i, j] + matrix2[i, j];

                return new Matrix(array);
            }

            public static Matrix operator -(Matrix matrix1, Matrix matrix2)
            {
                return (matrix1 + (matrix2 * -1));
            }

            public static bool operator ==(Matrix matrix1, Matrix matrix2)
            {
                if (ReferenceEquals(matrix1, null) || ReferenceEquals(matrix2, null))
                    throw new ArgumentNullException(ExceptionResource.NotEqualsRowsAndColumsForMultiplication);

                if (matrix1.Rows != matrix2.Rows || matrix1.Columns != matrix2.Columns)
                    return false;

                for (int i = 0; i < matrix1.Rows; i++)
                    for (int j = 0; j < matrix2.Rows; j++)
                        if (matrix1[i, j] != matrix2[i, j])
                            return false;
                return true;
            }

            public static bool operator !=(Matrix matrix1, Matrix matrix2)
            {
                return !(matrix1 == matrix2);
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                    return false;
                if (!(obj is Matrix))
                    return false;
                return (this == obj as Matrix);
            }

            /// <summary>
            /// Calculate inverse matrix by Gauss-Jordan method
            /// </summary>
            /// <returns>Inverse matrix</returns>
            public Matrix Inverse()
            {
                if (this.Rows != this.Columns)
                    throw new MatrixException(ExceptionResource.NotSquareMatrix);

                double[,] matrixCopy = (double[,])this.matrixArray.Clone();
                double[,] inversMatrix = new double[this.Rows, this.Columns];
                for (int i = 0; i < this.Rows; ++i)
                    inversMatrix[i, i] = 1.0;

                UpperTriangularMatrix(matrixCopy, inversMatrix);

                double multiplier;

                for (int i = this.Rows - 1; i > 0; i--)
                    for (int i2 = i - 1; i2 >= 0; i2--)
                    {
                        multiplier = matrixCopy[i2, i];
                        for (int j = 0; j < this.Rows; j++)
                        {
                            matrixCopy[i2, j] -= multiplier * matrixCopy[i, j];
                            inversMatrix[i2, j] -= multiplier * inversMatrix[i, j];
                        }
                    }

                return new Matrix(inversMatrix);
            }

            /// <summary>
            /// Calculate upper triangular matrix
            /// </summary>
            /// <param name="matrixCopy"></param>
            /// <param name="inversMatrix"></param>
            private void UpperTriangularMatrix(double[,] matrixCopy, double[,] inversMatrix)
            {
                bool rowsSwaped = false;
                double divider, multiplier;

                for (int i = 0; i < this.Rows; i++)
                {
                    if (matrixCopy[i, i] == (double)0)
                    {
                        for (int i2 = i + 1; i2 < this.Rows; i2++)
                        {
                            if (matrixCopy[i2, i] == (double)0)
                                continue;
                            SwapRows(matrixCopy, i, i2);
                            SwapRows(inversMatrix, i, i2);
                            rowsSwaped = true;
                            break;
                        }
                        if (!rowsSwaped)
                            throw new MatrixException(ExceptionResource.SingularMatrix);
                    }
                    divider = matrixCopy[i, i];
                    for (int j = 0; j < this.Rows; j++)
                    {
                        matrixCopy[i, j] /= divider;
                        inversMatrix[i, j] /= divider;
                    }
                    for (int i2 = i + 1; i2 < this.Rows; i2++)
                    {
                        multiplier = matrixCopy[i2, i];
                        for (int j = 0; j < this.Rows; j++)
                        {
                            matrixCopy[i2, j] -= multiplier * matrixCopy[i, j];
                            inversMatrix[i2, j] -= multiplier * inversMatrix[i, j];
                        }
                    }
                }
            }

            /// <summary>
            /// Swap rows in matrix
            /// </summary>
            /// <param name="matrix">matrix for swapping</param>
            /// <param name="row1"></param>
            /// <param name="row2"></param>
            private void SwapRows(double[,] matrix, int row1, int row2)
            {
                if (matrix == null)
                    throw new ArgumentNullException(ExceptionResource.NullMatrixReference);
                if (row1 == row2)
                    return;

                double temp;
                int columns = matrix.GetLength(1);
                for (int j = 0; j < columns; j++)
                {
                    temp = matrix[row1, j];
                    matrix[row1, j] = matrix[row2, j];
                    matrix[row2, j] = temp;
                }
            }
        }
    }
}
