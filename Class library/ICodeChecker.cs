﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    interface ICodeChecker
    {
        bool CodeCheckSyntax(string text, string language);
    }
}
