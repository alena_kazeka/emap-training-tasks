﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace MyException
    {
        namespace Task11Exception
        {
            public class StudentsTestException: Exception
            {
                public StudentsTestException(string message) : base(message) { }
            }
        }
    }
}
