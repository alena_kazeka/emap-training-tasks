﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace DelegatesTask
    {
        public class TickEventArgs : EventArgs
        {
            public readonly uint TickCount;
            public TickEventArgs(uint timeLeft)
            {
                TickCount = timeLeft;
            }
        }
    }
}
