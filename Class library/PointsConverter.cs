﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace EpamTrainingClassLibrary
{
    namespace Converter
    {
        /// <summary>
        /// The PointsConverter class allows convert point to required format
        /// </summary>
        public class PointsConverter
        {
            /// <summary>
            /// Pattern for data check
            /// </summary>
            static string pattern = @"^[-+]?\d+\.\d+\,[-+]?\d+\.\d+$";
            /// <summary>
            /// Regular expression for data check
            /// </summary>
            static Regex rgx = new Regex(pattern);

            ///<summary>
            ///Convert point to required format
            ///</summary>
            ///<param name="pointsString"> Gets a String that represents a point</param>
            ///<returns>A String that represents the converted points to required format</returns>    
            ///<example>
            ///<code lang="cs" outlining="true" title="Example of using the convertData(string pointsString) function">
            ///using System;
            ///using System.Text;
            ///
            ///public class Example
            ///{
            /// public static void Main()
            /// {
            ///     static void Main()
            ///     {
            ///         string point = "23.1242,54.2345";
            ///         string convertedPoint = convertData(point);
            ///         Console.WriteLine(convertedPoint); // X: 23,1242 Y: 54,2345
            ///     }
            /// }
            ///}
            ///</code>
            ///</example>
            public static string СonvertData(string pointsString)
            {
                if (pointsString == null || !rgx.IsMatch(pointsString))
                    return "Invalid data!";

                string[] points = pointsString.Split(',');
                for (int i = 0; i < points.Length; i++)
                    points[i].Replace('.', ',');

                return "X: " + points[0] + " Y: " + points[1];
            }

            ///<summary>
            ///Convert array of points to required format
            ///</summary>
            ///<param name="pointsArray">Gets a string array that represents a point</param>
            ///<returns>A String array that represents the converted points array to required format</returns>
            public static string[] СonvertData(string[] pointsArray)
            {
                if (pointsArray == null)
                    return (new string[] { "Invalid data!" });

                string[] convertedArray = new string[pointsArray.Length];
                for (int i = 0; i < pointsArray.Length; i++)
                {
                    convertedArray[i] = СonvertData(pointsArray[i]);
                }
                return convertedArray;
            }

            ///<summary>
            ///Convert array of points from file to required format
            ///</summary>
            ///<param name="filePath">Gets a string that represents the path to file that holds the points coords</param>
            ///<returns>A String array that represents the converted points array to required format</returns>
            public static string[] СonvertDataFromFile(string filePath)
            {
                try
                {
                    string[] pointsArray = File.ReadAllLines(filePath);
                    return СonvertData(pointsArray);
                }
                catch (FileNotFoundException exeption)
                {
                    return (new string[] { "Error", exeption.Message });
                }
            }
        }
    }
}
