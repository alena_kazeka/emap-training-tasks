﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using EpamTrainingClassLibrary.Exception.MathException;

namespace EpamTrainingClassLibrary
{
    namespace Math
    {
        public class GreatestCommonDivisor
        {
            public static uint EuclideanGCD(int firstNumber, int secondNumber)
            {
                uint firstNum = (uint)firstNumber;
                uint secondNum = (uint)secondNumber;

                while (firstNum != secondNum)
                {
                    if (firstNum > secondNum)
                        firstNum -= secondNum;
                    else
                        secondNum -= firstNum;
                }

                return firstNum;
            }
            public static uint EuclideanGCD(int firstNumber, int secondNumber, out double timeForCalculations)
            {
                uint firstNum = (uint)firstNumber;
                uint secondNum = (uint)secondNumber;

                Stopwatch watch = new Stopwatch();
                watch.Start();

                while (firstNum != secondNum)
                {
                    if (firstNum > secondNum)
                        firstNum -= secondNum;
                    else
                        secondNum -= firstNum;
                }

                watch.Stop();
                timeForCalculations = watch.Elapsed.TotalMilliseconds;

                return firstNum;
            }

            /// <summary>
            /// That method allows calculate greatest common divisor for array of numbers whit Euclid's algorithm
            /// </summary>
            /// <param name="numbers">It is an array of numbers for calculating greatest common divisor</param>
            /// <returns>This method returns greatest common divisor for two numbers</returns>
            public static uint EuclideanGCD(params int[] numbers)
            {
                if (numbers.Length < 2)
                    throw new GCDException("There must be at least two values");
                uint divisor = EuclideanGCD(numbers[0], numbers[1]);
                for (int i = 2; i < numbers.Length; i++)
                    divisor = EuclideanGCD((int)divisor, numbers[i]);
                return divisor;
            }

            /// <summary>
            /// That method allows calculate greatest common divisor for array of numbers whit Stein's algorithm
            /// </summary>
            /// <param name="firstNumber">It is a first number for calculating greatest common divisor</param>
            /// <param name="secondNumber">It is a second number for calculating greatest common divisor</param>
            /// <returns>This method returns greatest common divisor for two numbers</returns>
            public static uint SteinGCD(int firstNumber, int secondNumber)
            {
                uint firstNum = (uint)firstNumber;
                uint secondNum = (uint)secondNumber;

                if (firstNum == 0 && secondNum == 0)
                    return 0;
                if (firstNum == secondNum)
                    return (uint)firstNum;
                if (firstNum == 0)
                    return (uint)secondNum;
                if (secondNum == 0)
                    return (uint)firstNum;
                if (firstNum == 1 || secondNum == 1)
                    return 1;

                if (firstNum % 2 == 0 && secondNum % 2 == 0)
                    return 2 * SteinGCD((int)firstNum / 2, (int)secondNum / 2);

                if (firstNum % 2 == 0 && secondNum % 2 == 1)
                    return SteinGCD((int)firstNum / 2, (int)secondNum);

                if (firstNum % 2 == 1 && secondNum % 2 == 0)
                    return SteinGCD((int)firstNum, (int)secondNum / 2);

                if (firstNum % 2 == 1 && secondNum % 2 == 1 && firstNum < secondNum)
                    return SteinGCD((int)(secondNum - firstNum) / 2, (int)firstNum);
                else
                    return SteinGCD((int)(firstNum - secondNum) / 2, (int)secondNum);
            }

            public static uint SteinGCD(int firstNumber, int secondNumber, out double timeForCalculations)
            {
                uint firstNum = (uint)System.Math.Abs(firstNumber);
                uint secondNum = (uint)System.Math.Abs(secondNumber);

                Stopwatch watch = new Stopwatch();
                watch.Start();

                if (firstNum == 0 && secondNum == 0)
                {
                    watch.Stop();
                    timeForCalculations = watch.Elapsed.TotalMilliseconds;
                    return 0;
                }
                if (firstNum == secondNum)
                {
                    watch.Stop();
                    timeForCalculations = watch.Elapsed.TotalMilliseconds;
                    return (uint)firstNum;
                }
                if (firstNum == 0)
                {
                    watch.Stop();
                    timeForCalculations = watch.Elapsed.TotalMilliseconds;
                    return (uint)secondNum;
                }
                if (secondNum == 0)
                {
                    watch.Stop();
                    timeForCalculations = watch.Elapsed.TotalMilliseconds;
                    return (uint)firstNum;
                }
                if (firstNum == 1 || secondNum == 1)
                {
                    watch.Stop();
                    timeForCalculations = watch.Elapsed.TotalMilliseconds;
                    return 1;
                }

                int shift;

                for (shift = 0; ((firstNum | secondNum) & 1) == 0; ++shift)
                {
                    firstNum >>= 1;
                    secondNum >>= 1;
                }

                while ((firstNum & 1) == 0)
                    firstNum >>= 1;

                uint tempForSwap;

                do
                {
                    while ((secondNum & 1) == 0)
                        secondNum >>= 1;
                    if (firstNum > secondNum)
                    {
                        tempForSwap = secondNum;
                        secondNum = firstNum;
                        firstNum = tempForSwap;
                    }
                    secondNum -= firstNum;
                }
                while (secondNum != 0);

                firstNum <<= shift;

                watch.Stop();
                timeForCalculations = watch.Elapsed.TotalMilliseconds;

                return firstNum;
            }
        }
    }
}

