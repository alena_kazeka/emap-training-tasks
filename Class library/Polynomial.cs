﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpamTrainingClassLibrary.MyException.MathException;

namespace EpamTrainingClassLibrary
{
    namespace Mathematic
    {
        /// <summary>
        /// The Polynomial class allows create polynomial
        /// </summary>
        public class Polynomial
        {
            /// <summary>
            /// The elements of ring in polynomial.
            /// </summary>
            double[] elementsOfRing;

            public double[] ElementsOfRing
            {
                get
                {
                    return (double[])elementsOfRing.Clone();
                }
                set
                {
                    if (value == null)
                        throw new ArgumentNullException();
                    elementsOfRing = value;
                }
            }

            /// <summary>
            /// Creates an Polynomial instance with given elements.
            /// </summary>
            /// <param name="_ring">The elements of ring.</param>
            public Polynomial(double[] _ring)
            {
                if (_ring == null)
                    throw new ArgumentNullException();

                elementsOfRing = new double[_ring.Length];
                for (int i = 0; i < _ring.Length; i++)
                    elementsOfRing[i] = _ring[i];
            }

            /// <summary>
            /// Creates an Polynomial instance with maximum degree and constant ring value.
            /// </summary>
            /// <param name="maxDegree">The maximum degree in polynomial.</param>
            /// <param name="_ring">The value of ring.</param>
            public Polynomial(int maxDegree, double _ring)
            {
                if (maxDegree == 0 && _ring == 0)
                    throw new ArgumentNullException();

                elementsOfRing = new double[maxDegree+1];
                for (int i = 0; i < elementsOfRing.Length; i++)
                    elementsOfRing[i] = _ring;
            }

            public double this[int index]
            {
                get
                {
                    if (index >= 0 && index < elementsOfRing.Length)
                        return elementsOfRing[index];
                    throw new PolynomialException(ExceptionResource.IncorrectIndex);
                }
                set
                {
                    if (index < 0 || index >= elementsOfRing.Length)
                        throw new PolynomialException(ExceptionResource.IncorrectIndex);
                    elementsOfRing[index] = value;
                }
            }

            public override string ToString()
            {
                StringBuilder polynomialStr = new StringBuilder();
                for (int i = elementsOfRing.Length - 1; i >= 0; i--)
                {
                    polynomialStr.Append(elementsOfRing[i].ToString() + "*x^" + i.ToString());
                    if (i != 0)
                        polynomialStr.Append(" + ");
                }
                return polynomialStr.ToString();
            }

            public static bool operator ==(Polynomial polynomial1, Polynomial polynomial2)
            {
                if (polynomial1 == null || polynomial2 == null)
                    throw new ArgumentNullException();

                if (polynomial1.elementsOfRing.Length != polynomial2.elementsOfRing.Length)
                    return false;
                for (int i = 0; i < polynomial1.elementsOfRing.Length; i++)
                    if (polynomial1.elementsOfRing[i] != polynomial2.elementsOfRing[i])
                        return false;

                return true;
            }

            public static bool operator !=(Polynomial polynomial1, Polynomial polynomial2)
            {
                return !(polynomial1 == polynomial2);
            }

            public static bool operator >(Polynomial polynomial1, Polynomial polynomial2)
            {
                if (polynomial1 == null || polynomial2 == null)
                    throw new ArgumentNullException();

                if (polynomial1.elementsOfRing.Length > polynomial2.elementsOfRing.Length)
                    return true;
                else if (polynomial1.elementsOfRing.Length < polynomial2.elementsOfRing.Length)
                    return false;

                for (int i = polynomial1.elementsOfRing.Length - 1; i >= 0; i--)
                    if (polynomial1.elementsOfRing[i] < polynomial2.elementsOfRing[i])
                        return false;

                return true;
            }

            public static bool operator <(Polynomial polynomial1, Polynomial polynomial2)
            {
                return !(polynomial1 > polynomial2);
            }
        }
    }
}
