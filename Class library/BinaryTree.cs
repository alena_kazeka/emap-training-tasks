﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace MyCollection
    {
        /// <summary>
        /// Represents a binary tree of objects.
        /// </summary>
        /// <typeparam name="T">The type of elements in the binary tree</typeparam>
        public class BinaryTree<T> : IEnumerable<T>, ICloneable, ICollection<T>  where T : IComparable 
        {
            /// <summary>
            /// Node of tree
            /// </summary>
            class Node
            {
                /// <summary>
                /// Initializes a new instance of the Node class
                /// </summary>
                /// <param name="value">The object that inizializes the node</param>
                public Node(T value)
                {
                    Value = value;
                }
                public T Value
                {
                    get;
                    set;
                }

                /// <summary>
                /// The left child of current node
                /// </summary>
                public Node Left
                {
                    get;
                    set;
                }

                /// <summary>
                /// The right child of tree
                /// </summary>
                public Node Right
                {
                    get;
                    set;
                }                
            }

            /// <summary>
            /// Root of tree
            /// </summary>
            Node root;

            /// <summary>
            /// Initialize a new instance of BinaryTree class
            /// </summary>
            public BinaryTree() {}

            /// <summary>
            /// Initialize a new instance of BinaryTree class with given nodes
            /// </summary>
            /// <param name="array">Array of objects that represents nodes of tree</param>
            public BinaryTree(T[] array)
            {
                if (array == null)
                    throw new ArgumentNullException(ExceptionResource.NullArrayReference);

                for (int i = 0; i < array.Length; i++)
                    this.Add(array[i]);
            }

            /// <summary>
            /// Count of nodes in tree
            /// </summary>
            public int Count { get; private set; }

            /// <summary>
            ///  Gets a value indicating whether the BinaryTree<T> is read-only.
            /// </summary>
            public bool IsReadOnly
            { get { return false; } }

            /// <summary>
            /// Adds an item to the tree
            /// </summary>
            /// <param name="item">The object to add to the tree</param>
            public virtual void Add(T item)
            {
                var node = new Node(item);

                Count += 1;
                if (root == null)
                {
                    root = node;
                    return;
                }

                Node current = root, parent = null;
                while (current != null)
                {
                    parent = current;
                    if (item.CompareTo(current.Value) < 0)
                        current = current.Left;
                    else
                        current = current.Right;
                }

                if (item.CompareTo(parent.Value) < 0)
                    parent.Left = node;
                else
                    parent.Right = node;             
            }

            /// <summary>
            /// Removes all items from the tree
            /// </summary>
            public void Clear()
            {
                root = null;
                Count = 0;
            }

            /// <summary>
            /// Determines whether the binary tree contains a specific value.
            /// </summary>
            /// <param name="item">The object to locate in the binary tree</param>
            /// <returns>true if item is found in the binary tree; otherwise, false.</returns>
            public bool Contains(T item)
            {
                var current = root;
                while (current != null)
                {
                    var result = item.CompareTo(current.Value);
                    if (result == 0)
                        return true;
                    if (result > 0)
                        current = current.Right;
                    else
                        current = current.Left;
                }
                return false;
            }
                      
            /// <summary>
            /// Copies the elements of the binary tree to an System.Array, starting at a particular System.Array index.
            /// </summary>
            /// <param name="array">The one-dimensional System.Array that is the destination of the elements copied from BinaryTree<T>. 
            /// The System.Array must have zero-based indexing.</param>
            /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
            public void CopyTo(T[] array, int arrayIndex)
            {
                foreach (var value in this)
                    array[arrayIndex++] = value;
            }
            
            /// <summary>
            /// Removes the first occurrence of a specific object from the binary tree.
            /// </summary>
            /// <param name="item">The object to remove from the binary tree.</param>
            /// <returns></returns>
            public virtual bool Remove(T item)
            {
                if (root == null)
                    return false;

                Node current = root, parent = null;

                int compareResult;
                do
                {
                    compareResult = item.CompareTo(current.Value);
                    if (compareResult < 0)
                    {
                        parent = current;
                        current = current.Left;
                    }
                    else if (compareResult > 0)
                    {
                        parent = current;
                        current = current.Right;
                    }
                    if (current == null)
                        return false;
                }
                while (compareResult != 0);

                if (current.Right == null)
                {
                    if (current == root)
                        root = current.Left;
                    else
                    {
                        compareResult = current.Value.CompareTo(parent.Value);
                        if (compareResult < 0)
                            parent.Left = current.Left;
                        else
                            parent.Right = current.Left;
                    }
                }
                else if (current.Right.Left == null)
                {
                    current.Right.Left = current.Left;
                    if (current == root)
                        root = current.Right;
                    else
                    {
                        compareResult = current.Value.CompareTo(parent.Value);
                        if (compareResult < 0)
                            parent.Left = current.Right;
                        else
                            parent.Right = current.Right;
                    }
                }
                else
                {
                    Node min = current.Right.Left, prev = current.Right;
                    while (min.Left != null)
                    {
                        prev = min;
                        min = min.Left;
                    }
                    prev.Left = min.Right;
                    min.Left = current.Left;
                    min.Right = current.Right;

                    if (current == root)
                        root = min;
                    else
                    {
                        compareResult = current.Value.CompareTo(parent.Value);
                        if (compareResult < 0)
                            parent.Left = min;
                        else
                            parent.Right = min;
                    }
                }
                Count -= 1;
                return true;
            }
           
            /// <summary>
            /// Goes through a binary tree in ascending order.
            /// </summary>
            /// <returns>The enumerator, which supports a iteration over a collection in ascending order.</returns>
            public IEnumerable<T> Inorder()
            {
                if (root == null)
                    yield break;

                var stack = new Stack<Node>();
                var node = root;

                while (stack.Count > 0 || node != null)
                {
                    if (node == null)
                    {
                        node = stack.Pop();
                        yield return node.Value;
                        node = node.Right;
                    }
                    else
                    {
                        stack.Push(node);
                        node = node.Left;
                    }
                }
            }

            /// <summary>
            /// Goes through a binary tree in order root, left subtree, right subtree.
            /// </summary>
            /// <returns>The enumerator, which supports a iteration over a collection in order root, left subtree, right subtree.</returns>
            public IEnumerable<T> Preorder()
            {
                if (root == null)
                    yield break;

                var stack = new Stack<Node>();
                stack.Push(root);

                while (stack.Count > 0)
                {
                    var node = stack.Pop();
                    yield return node.Value;
                    if (node.Right != null)
                        stack.Push(node.Right);
                    if (node.Left != null)
                        stack.Push(node.Left);
                }
            }

            /// <summary>
            /// Goes through a binary tree in order left subtree, right subtree, root.
            /// </summary>
            /// <returns>The enumerator, which supports a iteration over a collection in order left subtree, right subtree, root.</returns>
            public IEnumerable<T> Postorder()
            {
                if (root == null)
                    yield break;

                var stack = new Stack<Node>();
                var node = root;

                while (stack.Count > 0 || node != null)
                {
                    if (node == null)
                    {
                        node = stack.Pop();
                        if (stack.Count > 0 && node.Right == stack.Peek())
                        {
                            stack.Pop();
                            stack.Push(node);
                            node = node.Right;
                        }
                        else
                        {
                            yield return node.Value;
                            node = null;
                        }
                    }
                    else
                    {
                        if (node.Right != null)
                            stack.Push(node.Right);
                        stack.Push(node);
                        node = node.Left;
                    }
                }
            }

            /// <summary>
            /// Returns an enumerator that iterates through the collection.
            /// </summary>
            /// <returns>A System.Collections.Generic.IEnumerator<T> that can be used to iterate through
            ///     the collection.</returns>
            public IEnumerator<T> GetEnumerator()
            {
                return Inorder().GetEnumerator();
            }
            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
            
            /// <summary>
            /// Creates a new object that is a copy of the current instance.
            /// </summary>
            /// <returns>A new object that is a copy of this instance.</returns>
            public object Clone()
            {
                BinaryTree<T> tree = new BinaryTree<T>();
                foreach (var value in this)                 
                    tree.Add(value);
                return tree;
            }

            /// <summary>
            /// Returns an array that contains nodes of binary tree.
            /// </summary>
            /// <returns>Returns an array that contains nodes of binary tree.</returns>
            public T[] ToArray()
            {
                T[] array = new T[Count];
                int index = 0;
                foreach (var value in this)
                    array[index++] = value;
                return array;
            }
        }
    }
}
