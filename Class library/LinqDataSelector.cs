﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using EpamTrainingClassLibrary.MyCollection;
using EpamTrainingClassLibrary.Task11;
using EpamTrainingClassLibrary.MyException.Task12Exception;

namespace EpamTrainingClassLibrary
{
    namespace Task12
    {
        /// <summary>
        /// The LinqDataSelector class contains methods for data of StudentsTest object selection.
        /// </summary>
        public class LinqDataSelector
        {
            /// <summary>
            /// Selects the best students of given test.
            /// </summary>
            /// <param name="test">The test that passed students.</param>
            /// <returns>The best student of test</returns>
            static public IEnumerable<Student> BestStudentsOfTest(StudentsTest test)
            {
                return test.Students.Where(st => st.TestMark == test.Students.Max(st2 => st2.TestMark));
            }

            /// <summary>
            /// Selects tests with greater than leastAverageMark average mark.
            /// </summary>
            /// <param name="tests">The BinaryTree collection of StudentsTest objects.</param>
            /// <param name="percent">The least mark for test</param>
            /// <returns>The collection of StudentsTest objects that have greater than leastAverageMark average mark.</returns>
            static public IEnumerable<StudentsTest> TestsWithGreaterAverageMark(BinaryTree<StudentsTest> tests, double leastAverageMark)
            {
                if (leastAverageMark < 0 || leastAverageMark > 100)
                    throw new LinqDataSelectorException(ExceptionResource.PercentValueException);

                return tests.Where(test => test.AverageMark > leastAverageMark);
            }

            /// <summary>
            /// Selects an Student objects than have given name.
            /// </summary>
            /// <param name="test">The test that passed students.</param>
            /// <param name="studentName">The student name.</param>
            /// <returns>The collection of Student objects whit name studentName.</returns>
            static public IEnumerable<Student> MarkOfStudent(StudentsTest test, string studentName)
            {
                ParameterExpression param = Expression.Parameter(typeof(Student), "student");
                MemberExpression currentName = Expression.Property(param, "Name");
                ConstantExpression stName = Expression.Constant(studentName);
                BinaryExpression equal = Expression.Equal(currentName, stName);

                Expression<Func<Student, bool>> lambda = Expression.Lambda<Func<Student, bool>>(equal, param);
                return test.Students.Where(lambda.Compile());
            }
        }

    }
}
