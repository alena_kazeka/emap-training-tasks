﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace Task11
    {
        public interface IProductA
        {
            double DiscountPrice(double discount);
        }
    }
}
