﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpamTrainingClassLibrary.Exception.MathException;

namespace EpamTrainingClassLibrary
{
    namespace Math
    {
        public class Pow
        {
            /// <summary>
            /// The First value of x for Newton  method function
            /// </summary>
            private static double x0 = 0.5;

            /// <summary>
            /// Calculates root from the number
            /// </summary>
            /// <param name="value"></param>
            /// <param name="n"></param>
            /// <param name="e"></param>
            /// <returns></returns>
            public static double GetPow(double value, double n, double e = 0.000001)
            {
                if (n == 0)
                    throw new PowException("When the degree of root is zero," +
                        "the answer can be any number!");
                if (value == 1 || value == 0)
                    return value;
                if (value == -1 && n % 2 == 1)
                    return -1;
                if (value < 0 && n % 2 == 0)
                    throw new PowException("When the number is negative and " +
                        "the degree of root is even, can't calculate root from the number!");

                int sign = 1;
                if (value < 0 && n % 2 == 1)
                    sign = -1;

                double x = x0;
                double xk;
                do
                {
                    xk = x;
                    x = 1 / n * ((n - 1) * xk + System.Math.Abs(value) / System.Math.Pow(xk, (n - 1)));
                }
                while (System.Math.Abs(x - xk) > e);
                return x*sign;
            }

            public static double GetPow(double value, double n, out double standartMathPowCompare,double e = 0.000001)
            {
                double rootFromValue = GetPow(value,n,e);
                standartMathPowCompare = System.Math.Pow(value, 1 / n) - rootFromValue;
                return rootFromValue;
            }
        }
    }
}
