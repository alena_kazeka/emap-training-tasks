﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using EpamTrainingClassLibrary.MyCollection;
using EpamTrainingClassLibrary.Task11;

namespace EpamTrainingClassLibrary
{
    namespace Task12
    {
        [Serializable]
        public class StudentsTestTreeSerialisatorFrame
        {
            public StudentsTestTreeSerialisatorFrame(BinaryTree<StudentsTest> tests)
            {
                this.tests = (BinaryTree<StudentsTest>)tests.Clone();
            }

            [NonSerialized]
            BinaryTree<StudentsTest> tests;
            public BinaryTree<StudentsTest> Tests
            {
                get { return tests; }
            }

            StudentsTest[] studentTestToSerialize;

            [OnSerializing]
            void OnSerializing(StreamingContext context)
            {
                studentTestToSerialize = tests.ToArray();
            }

            [OnSerialized]
            void OnSerialized(StreamingContext context)
            {
                studentTestToSerialize = null;
            }

            [OnDeserialized]
            void OnDeserialized(StreamingContext context)
            {
                tests = new BinaryTree<StudentsTest>(studentTestToSerialize);
            }
        }
    }
}
