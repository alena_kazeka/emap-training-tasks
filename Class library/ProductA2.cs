﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace Task11
    {
        public class ProductA2 : IProductA
        {
            string name;
            public string Name
            {
                get { return name; }
            }

            public ProductA2(string name)
            {
                this.name = name;
            }

            double price;
            public double Price
            {
                get { return price; }
                set { price = value; }
            }

            public double DiscountPrice(double discount)
            {
                return (price - discount * price);
            }
        }
    }
}
