﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace Exception
    {
        namespace MathException
        {
            public class PowException: System.Exception
            {
                public PowException(string message) : base(message) { }
            }
        }
    }
}
