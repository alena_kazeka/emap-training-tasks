﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using EpamTrainingClassLibrary.Task11;
using EpamTrainingClassLibrary.MyCollection;
using EpamTrainingClassLibrary.MyException.Task11Exception;

namespace EpamTrainingClassLibrary
{
    namespace Task11
    {
        /// <summary>
        /// The StudentsTest class represents class that describes test that students passed. 
        /// </summary>
        [Serializable]
        public class StudentsTest : IComparable
        {
            /// <summary>
            /// The name of test
            /// </summary>
            string name;
            public string Name
            {
                get { return name; }
            }

            /// <summary>
            /// The date of passing the test.
            /// </summary>
            DateTime date;
            public DateTime Date
            {
                get { return date; }
            }

            /// <summary>
            /// The collection of Student class objects that passed the test.
            /// </summary>
            [NonSerialized]
            BinaryTree<Student> students;
            public BinaryTree<Student> Students
            {
                get { return (BinaryTree<Student>)students.Clone(); }
            }

            /// <summary>
            /// The array of Student class objects for correct serialization.
            /// </summary>
            Student[] studentsToSerialize;

            [OnSerializing]
            void OnSerializing(StreamingContext context)
            {
                studentsToSerialize = students.ToArray();
            }

            [OnSerialized]
            void OnSerialized(StreamingContext context)
            {
                studentsToSerialize = null;
            }

            [OnDeserialized]
            void OnDeserialized(StreamingContext context)
            {
                students = new BinaryTree<Student>(studentsToSerialize);
            }
            public StudentsTest(string name, DateTime date, BinaryTree<Student> students)
            {
                if (name == null || date == null || students == null)
                    throw new ArgumentException(ExceptionResource.NullReferences);

                this.name = name;
                this.date = date;
                this.students = (BinaryTree<Student>)students.Clone();
                AverageMark = this.CalculateAverageMark();
            }

            /// <summary>
            /// The average mark for the test. 
            /// </summary>
            public double AverageMark
            {
                get;
                private set;
            }

            private double CalculateAverageMark()
            {
                double sum = 0;
                foreach (Student s in students)
                    sum += s.TestMark;
                return sum / students.Count;
            }
            public int CompareTo(object obj)
            {
                if (obj == null)
                    throw new ArgumentNullException(ExceptionResource.NullReferences);
                if (!(obj is StudentsTest))
                    throw new StudentsTestException(ExceptionResource.NotStudentsTestInstance);

                if (this.AverageMark > ((StudentsTest)obj).AverageMark)
                    return 1;
                if (this.AverageMark < ((StudentsTest)obj).AverageMark)
                    return -1;
                else
                    return 0;
            }
        }
    }
}
