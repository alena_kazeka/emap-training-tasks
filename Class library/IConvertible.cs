﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    interface IConvertible
    {
        string ConvertToCSharp(string text);
        string ConvertToVB(string text);
    }
}
