﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using EpamTrainingClassLibrary.MyCollection;
using EpamTrainingClassLibrary.MyException.Task11Exception;

namespace EpamTrainingClassLibrary
{
    namespace Task11
    {
        /// <summary>
        /// The StudentsTest class represents class that contains name of student and his test mark.
        /// </summary>
        [Serializable]
        public class Student : IComparable
        {
            /// <summary>
            /// The name of student.
            /// </summary>
            string name;
            public string Name
            {
                get { return name; }
            }
            
            /// <summary>
            /// Creates an instance of Student class whit given name and test mark.
            /// </summary>
            /// <param name="name">The name of student.</param>
            /// <param name="mark">The test mark.</param>
            public Student(string name, double mark)
            {
                if (name == null)
                    throw new ArgumentNullException(ExceptionResource.NullReferences);

                this.name = name;
                this.testMark = mark;
            }

            /// <summary>
            /// The test mark.
            /// </summary>
            double testMark;
            public double TestMark
            {
                get { return testMark; }
                set { testMark = value; }
            }

            public int CompareTo(object obj)
            {
                if (obj == null)
                    throw new ArgumentNullException(ExceptionResource.NullObjectInCompare);
                if (!(obj is Student))
                    throw new StudentException(ExceptionResource.NotStudentInstance);

                if (this.testMark > ((Student)obj).TestMark)
                    return 1;
                if (this.testMark < ((Student)obj).TestMark)
                    return -1;
                else
                    return 0;
            }
        }
    }
}
