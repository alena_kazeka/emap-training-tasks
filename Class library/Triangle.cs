﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpamTrainingClassLibrary.MyException.FigureException;

namespace EpamTrainingClassLibrary
{
    namespace Figure
    {
        /// <summary>
        /// The struct Point represent point with coords x, y and z
        /// </summary>
        public struct Point
        {
            public int x;
            public int y;
            public int z;

            public Point(int _x, int _y, int _z = 0)
            {
                x = _x;
                y = _y;
                z = _z;
            }
            public static bool operator ==(Point p1, Point p2)
            {
                if (p1.x == p2.x && p1.y == p2.y && p1.z == p2.z)
                    return true;
                return false;
            }
            public static bool operator !=(Point p1, Point p2)
            {
                return !(p1 == p2);
            }

            /// <summary>
            /// Calculates distance from first point to second.
            /// </summary>
            /// <param name="p1">The first point.</param>
            /// <param name="p2">The second point.</param>
            /// <returns>The distance between two points.</returns>
            public static double distance(Point p1, Point p2)
            {
                int x = Math.Abs(p1.x - p2.x);
                int y = Math.Abs(p1.y - p2.y);

                return Math.Pow(x * x + y * y, 0.5);
            }
        }

        /// <summary>
        /// The class Triangle allows create triangle and calculate for it perimeter and area.
        /// </summary>
        public class Triangle
        {
            /// <summary>
            /// Creates an Triangle class instance with given lengths of sides.
            /// </summary>
            /// <param name="a">The length of first side.</param>
            /// <param name="b">The length of second side.</param>
            /// <param name="c">The length of third side.</param>
            public Triangle(double a, double b, double c)
            {
                if (!IsTriangle(a, b, c))
                    throw new TriangleException(ExceptionResource.TriangleNotExistException);

                this.sideA = a;
                this.sideB = b;
                this.sideC = c;
            }

            /// <summary>
            /// Creates an Triangle class instance using given points.
            /// </summary>
            /// <param name="p1">The first point.</param>
            /// <param name="p2">The second point.</param>
            /// <param name="p3">The third point.</param>
            public Triangle(Point p1, Point p2, Point p3)
            {
                double sideA = Point.distance(p1, p2);
                double sideB = Point.distance(p2, p3);
                double sideC = Point.distance(p3, p1);

                if (!IsTriangle(sideA, sideB, sideC))
                    throw new TriangleException(ExceptionResource.TriangleNotExistException);

                this.sideA = sideA;
                this.sideB = sideB;
                this.sideC = sideC;
            }

            /// <summary>
            /// The first triangle side.
            /// </summary>
            double sideA;

            public double SideA
            {
                get { return sideA; }
                set 
                { 
                    if (!IsTriangle(value, sideB, sideC))
                        throw new TriangleException(ExceptionResource.TriangleNotExistException);
                    sideA = value; 
                }
            }

            /// <summary>
            /// The second triangle side.
            /// </summary>
            double sideB;

            public double SideB
            {
                get { return sideB; }
                set 
                {
                    if (!IsTriangle(sideA, value, sideC))
                        throw new TriangleException(ExceptionResource.TriangleNotExistException);
                    sideB = value; 
                }
            }

            /// <summary>
            /// The third triangle side.
            /// </summary>
            double sideC;

            public double SideC
            {
                get { return sideC; }
                set 
                {
                    if (!IsTriangle(sideA, sideB, value))
                        throw new TriangleException(ExceptionResource.TriangleNotExistException);
                    sideC = value; 
                }
            }

            /// <summary>
            /// Checks that exists triangle with given sides.
            /// </summary>
            /// <param name="a">The length of first side.</param>
            /// <param name="b">The length of second side.</param>
            /// <param name="c">The length of third side.</param>
            /// <returns>true: triange exists; otherwise false.</returns>
            bool IsTriangle(double a, double b, double c)
            {
                if (a + b < c || a + c < b || c + b < a)
                    return false;
                return true;
            }

            /// <summary>
            /// Checks that two Triangle objects have equal area.
            /// </summary>
            /// <param name="tr1">The first triangle.</param>
            /// <param name="tr2">The second triangle.</param>
            /// <returns>true: Triangle objects have equal area; otherwise false.</returns>
            public static bool EqualArea(Triangle tr1, Triangle tr2)
            {
                if (tr1 == null || tr2 == null)
                    throw new ArgumentNullException();

                double areaTr1 = tr1.Area();
                double areatr2 = tr2.Area();
                if (areaTr1 == areatr2)
                    return true;
                return false;
            }

            /// <summary>
            /// Calculates the perimeter of current triangle.
            /// </summary>
            /// <returns>The perimeter of current triangle.</returns>
            public double Perimeter()
            {
                return (sideA + sideB + sideC);
            }

            /// <summary>
            /// Calculates the area of current triangle.
            /// </summary>
            /// <returns>The area of current triangle.</returns>
            public double Area()
            {
                double halfPerimeter = (sideA + sideB + sideC) / 2;
                return Math.Sqrt(halfPerimeter*(halfPerimeter-sideA)*(halfPerimeter-sideB)*(halfPerimeter-sideC));
            }
        }
    }
}
