﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace MyException
    {
        namespace MathException
        {
            public class PolynomialException: Exception
            {
                public PolynomialException(string message) : base(message) { }
            }
        }
    }
}
