﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    public class ProgramHelper: ProgramConverter, ICodeChecker
    {
        public bool CodeCheckSyntax(string text, string language)
        {
            if (text == null || language == null)
                throw new ArgumentNullException();

            Random rand = new Random();
            int result = rand.Next(0, 1);
            if (result == 1)
                return true;

            return false;
        }
    }
}
