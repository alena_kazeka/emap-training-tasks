﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using EpamTrainingClassLibrary.DelegatesTask;
using EpamTrainingClassLibrary.MyException.DelegatesTaskException;

namespace EpamTrainingClassLibrary
{
    namespace DelegatesTask
    {
        public delegate void TimeOutDelegate(object sender, EventArgs e);
        public class Timer
        {
            /// <summary>
            /// The size of one tick
            /// </summary>
            uint tickSize;
            public uint TickSize
            {
                get { return tickSize; }
            }
            /// <summary>
            /// Count of ticks
            /// </summary>
            uint tickCount;
            public uint TickCount
            {
                get { return tickCount; }
            }

            /// <summary>
            /// Creates an instance of Timer class
            /// </summary>
            /// <param name="tickCount">The count of ticks</param>
            /// <param name="tickSize">The size of one tick in milliseconds</param>
            public Timer(uint tickCount, uint tickSize)
            {
                this.tickSize = tickSize;
                this.tickCount = tickCount;
            }

            /// <summary>
            /// The timeout event
            /// </summary>
            public event TimeOutDelegate TimeOut;

            /// <summary>
            /// Starts timer
            /// </summary>
            public void Start()
            {
                OnTick(new TickEventArgs(tickCount));
                for (uint i = tickCount; i > 0; i--)
                {
                    Thread.Sleep((int)tickSize);
                    OnTick(new TickEventArgs(i-1));
                }
                OnTimeout(new TickEventArgs(0));
            }

            /// <summary>
            /// Callback method for timeout event.
            /// </summary>
            /// <param name="e">Current event parameters.</param>
            protected virtual void OnTimeout(TickEventArgs e)
            {
                if (TimeOut != null)
                    TimeOut(this, e);
            }

            /// <summary>
            /// The tick event.
            /// </summary>
            public event EventHandler<TickEventArgs> Tick;

            /// <summary>
            /// Callback method for tick event
            /// </summary>
            /// <param name="e">Current event parameters.</param>
            protected virtual void OnTick(TickEventArgs e)
            {
                if (Tick != null)
                    Tick(this, e);
            }
        }
    }
}
