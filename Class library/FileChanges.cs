﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using EpamTrainingClassLibrary.MyException.Task9Exception;

namespace EpamTrainingClassLibrary
{
    namespace Task9
    {
        public class FileChanges: FileStream, IDisposable
        {
            public FileChanges(string path, FileMode mode)
                : base(path, mode)
            { }

            public FileChanges(string path, FileMode mode, FileAccess access) : base(path, mode, access) { }

            /// <summary>
            /// Changes file by given parameters
            /// </summary>
            /// <param name="array">Bytes array for adding to file</param>
            /// <param name="offset">Offset in the file from which data will change</param>
            /// <param name="removedLength">Length of removed bytes</param>
            /// <param name="addedLength">Length of added bytes</param>
            public void Change(byte[] array, int offset, int removedLength, int addedLength)
            {
                if (disposed)
                    throw new ObjectDisposedException(ExceptionResource.StreamDisposed);

                this.Remove(offset, removedLength);
                this.Add(array, offset, addedLength);
            }

            /// <summary>
            /// Removes data from file.
            /// </summary>
            /// <param name="offset"></param>
            /// <param name="removedLength"></param>
            public void Remove(int offset, int removedLength)
            {
                if (offset > this.Length)
                    throw new Exception();

                if (removedLength == 0)
                    return;

                int symbol;
                if (removedLength > 0)
                {
                    for (int i = offset + removedLength; i < this.Length; i++)
                    {
                        this.Position = i;
                        symbol = this.ReadByte();
                        this.Position = i - removedLength;
                        this.WriteByte((byte)symbol);
                    }
                    this.SetLength(this.Length - removedLength);
                }
            }

            /// <summary>
            /// Addes data to file.
            /// </summary>
            /// <param name="array"></param>
            /// <param name="offset"></param>
            /// <param name="addedLength"></param>
            public void Add(byte[] array, int offset, int addedLength)
            {
                if (offset > this.Length)
                    throw new Exception();

                if (addedLength == 0)
                    return;

                int symbol;
                if (addedLength > 0)
                {
                    if (array == null)
                        throw new Exception();

                    this.SetLength(this.Length + addedLength);

                    for (long i = this.Length - 1; i > offset; i--)
                    {
                        this.Position = i - addedLength;
                        symbol = this.ReadByte();
                        this.Position = i;
                        this.WriteByte((byte)symbol);
                    }
                    for (int i = 0; i < addedLength; i++)
                    {
                        this.Position = offset + i;
                        this.WriteByte(array[i]);
                    }
                }
            }

            /// <summary>
            ///  Track whether Dispose has been called.
            /// </summary>
            private bool disposed = false;
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (this.disposed || !disposing)
                    return;

                if (this != null)
                {
                    this.Flush();
                    this.Close();
                }                
                disposed = true;
            }

            ~FileChanges()
            {
                Dispose(false);
            }
        }
    }
}
