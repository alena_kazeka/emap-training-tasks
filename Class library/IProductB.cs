﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EpamTrainingClassLibrary
{
    namespace Task11
    {
        public interface IProductB
        {
            double DiscountPrice(double discount);
        }
    }
}
