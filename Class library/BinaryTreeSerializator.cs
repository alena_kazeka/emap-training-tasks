﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using EpamTrainingClassLibrary.Task11;
using EpamTrainingClassLibrary.MyCollection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace EpamTrainingClassLibrary
{
    namespace Task12
    {
        /// <summary>
        /// The BinaryTreeSerializator class allows serialize and deserialize BinaryTree<StudentsTest> collection.
        /// </summary>
        public class BinaryTreeSerializator
        {
            static public void Serialize(BinaryTree<StudentsTest> studentsTests, string filePath = "serializedTestsTree.bin")
            {
                StudentsTestTreeSerialisatorFrame stTreeSerialization = new StudentsTestTreeSerialisatorFrame(studentsTests);
                BinaryFormatter formatter = new BinaryFormatter();
                using (FileStream s = File.Create(filePath))
                    formatter.Serialize(s, stTreeSerialization);
            }

            static public BinaryTree<StudentsTest> Deserialize(string filePath = "serializedTestsTree.bin")
            {
                StudentsTestTreeSerialisatorFrame stTreeSerializator;
                BinaryFormatter formatter = new BinaryFormatter();

                using (FileStream s = File.OpenRead("serializedTestsTree.bin"))
                    stTreeSerializator = (StudentsTestTreeSerialisatorFrame)formatter.Deserialize(s);

                return stTreeSerializator.Tests;
            }
        }
    }
}
